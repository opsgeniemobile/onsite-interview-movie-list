# Opsgenie Android Onsite Interview Template

This project template has been designed to help candidates set up a brand new Android project that works on the latest Android Studio build with
the latest Android SDK. Details of the project and the task should be provided by interviewers during the interview.

## TMDB v3

API docs are ![https://developers.themoviedb.org/3/getting-started/introduction](here).

Movie list project requires candidates to use TMDB API v3 which basically works with a simple API_KEY and can be tested on the browser.

An example request to fetch the popular movies list is

```
https://api.themoviedb.org/3/movie/popular?api_key=<API_KEY>&language=en-US&page=1
```

The response should look like

```
{
  "page": 1,
  "results": [
    {
      "adult": false,
      "backdrop_path": "/eENEf62tMXbhyVvdcXlnQz2wcuT.jpg",
      "genre_ids": [
        878,
        28,
        12
      ],
      "id": 580489,
      "original_language": "en",
      "original_title": "Venom: Let There Be Carnage",
      "overview": "After finding a host body in investigative reporter Eddie Brock, the alien symbiote must face a new enemy, Carnage, the alter ego of serial killer Cletus Kasady.",
      "popularity": 7405.695,
      "poster_path": "/rjkmN1dniUHVYAtwuV3Tji7FsDO.jpg",
      "release_date": "2021-09-30",
      "title": "Venom: Let There Be Carnage",
      "video": false,
      "vote_average": 7.2,
      "vote_count": 4546
    },
    {
      "adult": false,
      "backdrop_path": "/VlHt27nCqOuTnuX6bku8QZapzO.jpg",
      "genre_ids": [
        28,
        12,
        878,
        14
      ],
      "id": 634649,
      "original_language": "en",
      "original_title": "Spider-Man: No Way Home",
      "overview": "Peter Parker is unmasked and no longer able to separate his normal life from the high-stakes of being a super-hero. When he asks for help from Doctor Strange the stakes become even more dangerous, forcing him to discover what it truly means to be Spider-Man.",
      "popularity": 5687.424,
      "poster_path": "/1g0dhYtq4irTY1GPXvft6k4YLjm.jpg",
      "release_date": "2021-12-15",
      "title": "Spider-Man: No Way Home",
      "video": false,
      "vote_average": 8.7,
      "vote_count": 300
    },
    ...
  ],
  "total_pages": 31583,
  "total_results": 631655
}
```