package com.halilibo.opsgenieinterview_movielist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.halilibo.opsgenieinterview_movielist.BuildConfig.API_KEY

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}